import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PolygonTraversal {

	public long count(int N, int[] points) {
		if (points.length == 2) {
			return 0;
		}
		for(int i = 0; i < points.length; i++) {
			points[i]--;
		}
		init(N);

		Set<Integer> left = new HashSet<Integer>();
		for (int i = 0; i < N; i++) {
			left.add(i);
		}

		for (int i = 0; i < points.length - 1; i++) {
			add(N, points[i], points[i + 1]);
			left.remove(points[i] - 1);
		}
		left.remove(points[points.length - 1]);

		return dfs(N, points[0], points[points.length - 1], left);
	}

	long dfs(int N, int dest, int curr, Set<Integer> left) {
		if (left.isEmpty()) {
			return group.get(curr) == group.get(dest) ? 1 : 0;
		}
		long count = 0;
		for (int point : left) {
			if(group.get(curr) == group.get(point)) {
				left.remove(point);
				add(N, curr, point);
				count += dfs(N, dest, point, left);
				left.add(point);
				remove(N, curr, point);
			}
		}
		return count;
	}

	Map<Integer, Integer> group = new HashMap<Integer, Integer>();
	int nextGroup = 1;

	void init(int N) {
		for (int i = 0; i < N; i++) {
			group.put(i, 0);
		}
	}

	void add(int N, int from, int to) {
		if (from < to) {
			int temp = from;
			from = to;
			to = temp;
		}
		int fromGroup = group.get(from);
		int left = from + 1;
		while (from != to && group.get(left) == fromGroup) {
			group.put(left, nextGroup);
			left = (left + 1) % N;
		}
		nextGroup++;
		
		int toGroup = group.get(to);
		left = to + 1;
		while (to != from && group.get(left) == toGroup) {
			group.put(left, nextGroup);
			left = (left + 1) % N;
		}
		nextGroup++;
	}
	
	void remove(int N, int from, int to) {
		if (from < to) {
			int temp = from;
			from = to;
			to = temp;
		}
		
		int toGroup = group.get(to);
		int left = to + 1;
		while (to != from && group.get(left) == nextGroup - 1) {
			group.put(left, toGroup);
			left = (left + 1) % N;
		}
		nextGroup--;
		
		int fromGroup = group.get(from);
		left = from + 1;
		while (from != to && group.get(left) == nextGroup - 1) {
			group.put(left, fromGroup);
			left = (left + 1) % N;
		}
		nextGroup--;
	}
}
