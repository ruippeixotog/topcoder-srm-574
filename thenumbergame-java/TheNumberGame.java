public class TheNumberGame {

	public String determineOutcome(int A, int B) {
		String a = "" + A;
		String b = "" + B;
		return a.contains(b) || a.contains(reverse(b)) ? "Manao wins"
				: "Manao loses";
	}

	public String reverse(String str) {
		return new StringBuffer(str).reverse().toString();
	}
}
